Ext.define('MVVM.controller.Master', {
    extend : 'Ext.app.Controller',

    init: function() {
        this.control({
            'mvvm-RightOptionView > button#AddRecord' : {
                click : this.onAddRecordClick
            },
            'mvvm-RightOptionView > button#EditRecord' : {
                click : this.onEditRecordClick
            },
            'mvvm-RightOptionView > button#DeleteRecord' : {
                click : this.onDeleteRecordClick
            },
            'mvvm-LeftOptionView > textfield#FilterFamily' : {
                change : this.onChangeFilterFamily
            },
            'mvvm-LeftOptionView > combo#FilterSex' : {
                change : this.onChangeFilterSex
            }
        });
    },

    onAddRecordClick: function () {
        const modalWindow = Ext.ComponentQuery.query('mvvm-modalWindow')[0];
        modalWindow.show();
        modalWindow.add({
            xtype  : 'mvvm-DetailView',
            viewModel : {
                data : {
                    rec : null
                }
            }
        })
    },

    onEditRecordClick: function () {
        const selection = Ext.ComponentQuery.query('mvvm-MasterView')[0].getView().getSelectionModel().getSelection()[0];
        if (selection) {
            const modalWindow = Ext.ComponentQuery.query('mvvm-modalWindow')[0];
            modalWindow.show();
            modalWindow.add({
                xtype  : 'mvvm-DetailView',
                viewModel : {
                    data : {
                        rec : selection
                    }
                }
            })

        }
    },

    onDeleteRecordClick: function () {
        const selection = Ext.ComponentQuery.query('mvvm-MasterView')[0].getView().getSelectionModel().getSelection()[0];
        if (selection) {
            Ext.StoreManager.lookup('People').remove(selection);
        }
    },

    onChangeFilterFamily: function (input) {
        Ext.StoreManager.lookup('People').filter("family", input.value);
    },

    onChangeFilterSex: function (combo) {
        combo.value === 'All' ?  Ext.StoreManager.lookup('People').clearFilter() :
                                 Ext.StoreManager.lookup('People').filter("sex", combo.value);
    }
});
