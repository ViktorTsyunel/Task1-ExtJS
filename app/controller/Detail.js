Ext.define('MVVM.controller.Detail', {
    extend : 'Ext.app.Controller',

    init : function() {
        this.control({
            'mvvm-DetailView > fieldcontainer > button#SaveRecord' : {
                click : this.onSaveButtonClick
            },
            'mvvm-DetailView > fieldcontainer > button#Close' : {
                click : this.onCloseButtonClick
            },
        });
    },

    onSaveButtonClick : function(btn) {
        const record = btn.up('mvvm-DetailView').getViewModel().getData().rec;
        if (record) {
            Ext.StoreManager.lookup('People').insert(0, record);
        }
        const modalWindow = Ext.ComponentQuery.query('mvvm-modalWindow')[0];
        modalWindow.removeAll();
        modalWindow.hide();
    },

    onCloseButtonClick : function() {
        const modalWindow = Ext.ComponentQuery.query('mvvm-modalWindow')[0];
        modalWindow.removeAll();
        modalWindow.hide();
    }
});