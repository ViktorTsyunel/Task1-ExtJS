Ext.define('MVVM.model.Person', {
    extend : 'Ext.data.Model',

    fields: [{
        family: 'family',
        type: 'string'
    }, {
        name: 'name',
        type: 'string'
    }, {
        otch: 'otch',
        type: 'string'
    }, {
        age: 'age',
        type: 'string'
    }, {
        sex: 'sex',
        type: 'bool'
    }, {
        comment: 'comment',
        type: 'string'
    }]
});