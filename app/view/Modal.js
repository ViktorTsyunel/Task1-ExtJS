Ext.define('MVVM.view.Modal', {
    extend : 'Ext.window.Window',
    title: 'Добавление',
    width: 600,
    height: 220,
    autoHeight: true,
    autoScroll: true,
    closeAction: 'hide',
    closable: true,
    modal: true,
    headerPosition: 'top',
    bodyPadding: 10,
    xtype  : 'mvvm-modalWindow'
});
