Ext.define('MVVM.view.RightOption', {
    extend : 'Ext.panel.Panel',
    xtype  : 'mvvm-RightOptionView',
    layout: 'border',
    items : [
        {
            xtype  : 'button',
            region: 'east',
            text   : 'Добавить',
            itemId : 'AddRecord'
        }, {
            xtype  : 'button',
            region: 'east',
            text   : 'Редактировать',
            itemId : 'EditRecord'
        }, {
            xtype  : 'button',
            region: 'east',
            text   : 'Удалить',
            itemId : 'DeleteRecord'
        }
    ]
});