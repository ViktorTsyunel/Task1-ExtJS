Ext.define('MVVM.view.Option', {
    extend : 'Ext.panel.Panel',
    xtype  : 'mvvm-OptionView',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items : [
        {
            xtype: 'mvvm-LeftOptionView',
            width: 340
        },
        {
            xtype: 'mvvm-RightOptionView',
            flex: 1
        }
    ]
});