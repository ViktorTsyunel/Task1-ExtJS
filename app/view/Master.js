Ext.define('MVVM.view.Master', {
    extend : 'Ext.grid.Panel',
    xtype  : 'mvvm-MasterView',
    store : 'People',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    flex: 1,
    columns: [
        {
            text: 'Фамилия',
            dataIndex: 'family',
            flex: 1,
        }, {
            xtype: 'templatecolumn',
            tpl: '{name} - {otch}',
            text: 'Имя - Отчество',
            flex: 1,
            dataIndex: 'name'
        }, {
            text: 'Возраст',
            dataIndex: 'age',
            flex: 1,
        }, {
            xtype: 'booleancolumn',
            trueText: 'М',
            falseText: 'Ж',
            text: 'Пол',
            dataIndex: 'sex',
            flex: 1,
        }, {
            text: 'Комментарий',
            width: 120,
            dataIndex: 'comment',
            flex: 1,
        }
    ]
});