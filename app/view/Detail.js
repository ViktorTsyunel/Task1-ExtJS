Ext.define('MVVM.view.Detail', {
    extend : 'Ext.form.Panel',
    xtype  : 'mvvm-DetailView',

    requires : [
        'MVVM.view.DetailViewModel'
    ],

    border: false,

    bind : {
        reference : 'MVVM.model.Person'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items : [
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            items: [
                {
                    xtype      : 'textfield',
                    bind       : '{rec.family}',
                    fieldLabel: 'Фамилия',
                }, {
                    xtype: 'numberfield',
                    fieldLabel: 'Возраст',
                    bind       : '{rec.age}',
                    minValue: 1,
                }
            ]
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            items: [
                {
                    xtype: 'fieldcontainer',
                    layout: 'vbox',
                    items: [{
                            fieldLabel: 'Имя',
                            xtype      : 'textfield',
                            bind       : '{rec.name}',
                        }, {
                            fieldLabel: 'Отчество',
                            xtype      : 'textfield',
                            bind       : '{rec.otch}',
                        }
                    ],
                }, {
                    xtype: 'textareafield',
                    grow: true,
                    fieldLabel: 'Комментарий',
                    bind       : '{rec.comment}',
                }
            ]
        }, {
            xtype: 'fieldcontainer',
            defaultType: 'radiofield',
            layout: {type: 'hbox', pack:'center'},
            items: [
                {
                    boxLabel: 'М',
                    name: 'sex',
                    bind       : '{rec.sex}',
                }, {
                    boxLabel: 'Ж',
                    name: 'sex',
                    bind       : '{!rec.sex}',
                }
            ]
        }, {
            xtype: 'fieldcontainer',
            layout: {type: 'hbox', pack:'end'},
            items: [
                {
                    xtype  : 'button',
                    text   : 'Сохранить',
                    itemId : 'SaveRecord'
                },
                {
                    xtype  : 'button',
                    text   : 'Отмена',
                    itemId : 'Close'
                }
            ]
        }
    ]
});

