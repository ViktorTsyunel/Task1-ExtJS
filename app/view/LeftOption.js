Ext.define('MVVM.view.LeftOption', {
    extend : 'Ext.panel.Panel',
    xtype  : 'mvvm-LeftOptionView',
    layout: 'border',
    items : [
        {
            xtype  : 'textfield',
            region: 'west',
            itemId : 'FilterFamily'
        }, {
            xtype  : 'combo',
            region: 'west',
            store: 'Combo',
            queryMode: 'local',
            displayField: 'name',
            valueField: 'val',
            itemId : 'FilterSex'
        },
    ],
});