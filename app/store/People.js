Ext.define('MVVM.store.People', {
    extend  : 'Ext.data.Store',

    requires : [
        'MVVM.model.Person'
    ],

    storeId : 'People',
    model   : 'MVVM.model.Person',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'app/data/Users.json',
        reader: {
            type: 'json',
            root: 'users'
        }
    },
});