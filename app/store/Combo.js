Ext.define('MVVM.store.Combo', {
    extend  : 'Ext.data.Store',
    storeId : 'Combo',
    fields: ['val', 'name'],
    data: [{
        "val": 'All',
        "name": "Любой"
    }, {
        "val": true,
        "name": "Мужской"
    }, {
        "val": false,
        "name": "Женский"
    }]
});