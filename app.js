Ext.application({
    name : 'MVVM',

    views : [
        'Master',
        'Detail',
        'LeftOption',
        'RightOption',
        'Option',
        'Modal',
    ],

    controllers : [
        'Master',
        'Detail'
    ],

    stores : [
        'People',
        'Combo'
    ],

    launch : function() {
        Ext.create('Ext.container.Viewport', {
            layout : {
                type  : 'vbox',
                align : 'stretch'
            },
            items : [
                {
                    xtype : 'mvvm-OptionView' ,
                    height: 30,
                },
                {
                    xtype : 'mvvm-MasterView' ,
                    flex  : 1
                },
                {
                    xtype : 'mvvm-modalWindow',
                    flex  : 1
                }
            ]
        });
    }
});